# Fablab Project

## Camera.py

Gestore della camera. Acquisizione, elaborazione e salvataggio immagini ( gestisce la directory ).

## Predictions.py

Intelligenza artificiale per la classificazione delle immagini. Genera una predizione per ogni immagine acquisita.

## Game.py

Interfaccia verso i giochi. Usa le predizioni per procedere nel gioco specifico.