import logging
from picamera import PiCamera
from PIL import Image
from datetime import datetime

log = logging.getLogger(__name__)

TMP_IMG = '/tmp/image'

class Camera():
    def __init__(self):

        # init + setup camera
        self.camera = PiCamera()
        self.camera.resolution = (224,224)
        self.camera.shutter_speed = self.camera.exposure_speed
        self.camera.exposure_mode = 'off'
        


    def capture(self):
        log.debug('Cattura nuova immagine...')
        img_path = TMP_IMG+f"_{datetime.now().timestamp()}.jpg"
        self.camera.capture(img_path)
        log.debug('Immagine catturata in {}'.format(img_path))
        return Image.open(img_path)

    def show_preview(self):
        log.debug('Avvio preview dalla camera')

        self.camera.start_preview()
        self.camera.preview.fullscreen = False
        self.camera.preview.window = (0,0,300,300)
    
    def stop_preview(self):
        log.debug('Stop preview dalla camera')

        self.camera.stop_preview()
