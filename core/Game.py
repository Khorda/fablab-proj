import logging
from asyncio.tasks import sleep
from transitions import Machine
from core.Camera import Camera
from core.Display import Display
from core.Predictions import Predictions
import sys
from random import randint

log = logging.getLogger(__name__)

class Control:

    _FSM = (
        [
            'WELCOME',
            'CHOOSE',
            'TRIVIA',
            'MORRA',
            'TRIS',
            'EXIT'
        ],
        [
            ('play', 'WELCOME', 'CHOOSE'),
            ('stop', 'WELCOME', 'EXIT'),
            ('trivia', 'CHOOSE', 'TRIVIA'),
            ('morra', 'CHOOSE', 'MORRA'),
            ('tris', 'CHOOSE', 'TRIS'),
            ('stop', 'CHOOSE', 'EXIT')
        ]
    )

    _MEDIA_FOLDER = 'media/control/'

    def __init__(self) -> None:
        self.name = 'CONTROL'
        log.debug('Inizializzazione CONTROL...')
        self.p = None
        self.media_path = self._MEDIA_FOLDER + 'WELCOME.jpg'

    def on_enter_WELCOME(self):
        self.media_path = self._MEDIA_FOLDER + 'WELCOME.jpg'

    def on_enter_CHOOSE(self):
        self.media_path = self._MEDIA_FOLDER + 'CHOOSE.jpg'

    def on_enter_TRIVIA(self):
        self.media_path = self._MEDIA_FOLDER + 'TRIVIA.jpg'
    
    def on_enter_MORRA(self):
        self.media_path = self._MEDIA_FOLDER + 'MORRA.jpg'
    
    def on_enter_TRIS(self):
        self.media_path = self._MEDIA_FOLDER + 'TRIS.jpg'

    def p2a(self, prediction):
        self.p = prediction
        return prediction

class Trivia:

    _FSM = (
        ['WELCOME', 'QUESTION', 'EVAL', 'CORRECT', 'INCORRECT', 'GAME_OVER'],
        [
            ('play', 'WELCOME', 'QUESTION'),
            ('stop', 'WELCOME', 'GAME_OVER'),

            ('true', 'QUESTION', 'EVAL'),
            ('false', 'QUESTION', 'EVAL'),
            ('1', 'QUESTION', 'EVAL'),
            ('2', 'QUESTION', 'EVAL'),
            ('3', 'QUESTION', 'EVAL'),
            ('4', 'QUESTION', 'EVAL'),

            ('correct', 'EVAL', 'CORRECT'),
            ('incorrect', 'EVAL', 'INCORRECT'),

            ('play', 'CORRECT', 'QUESTION'),
            ('stop', 'CORRECT', 'GAME_OVER'),

            ('play', 'INCORRECT', 'QUESTION'),
            ('stop', 'INCORRECT', 'GAME_OVER'),
        ]
    )

    _MEDIA_FOLDER = 'media/trivia/'
    _QA = {
        'arte': [
            {'path': 'GOGH_VERO.jpg', 'ans': 'true'},
            {'path': 'GUERNICA_4.jpg', 'ans': '4'},
            {'path': 'PICASSO_VERO.jpg', 'ans': 'true'},
            ],
        'cinema': [
            {'path': 'FROZEN_VERO.jpg', 'ans': 'true'},
            {'path': 'PASSARE_2.jpg', 'ans': '2'},
            {'path': 'WONDER_FALSO.jpg', 'ans': 'false'},
        ],
        'logica': [
            {'path': 'ALBERI_VERO.jpg', 'ans': 'true'},
            {'path': 'NEWTON_VERO.jpg', 'ans': 'true'},
            {'path': 'OPERAZIONE_FALSO.jpg', 'ans': 'false'},
        ],
        'musica': [
            {'path': 'ALADDIN_4.jpg', 'ans': '4'},
            {'path': 'DESPACITO_3.jpg', 'ans': '3'},
            {'path': 'EBBASTA_1.jpg', 'ans': '1'},
        ]
    }

    def __init__(self) -> None:
        self.name = 'TRIVIA'
        log.debug('Inizializzazione classe TRIVIA...')
        self._done = set()
        self.score = 0
        self.scope = None
        self.qa = None
        self.p = None
        self.media_path = self._MEDIA_FOLDER + 'WELCOME.jpg'

    def on_enter_WELCOME(self):
        self.media_path = self._MEDIA_FOLDER + 'WELCOME.jpg'

    def on_enter_CORRECT(self):
        self.media_path = self._MEDIA_FOLDER + 'CORRECT.jpg'
        self.score += 1

    def on_enter_INCORRECT(self):
        self.media_path = self._MEDIA_FOLDER + 'INCORRECT.jpg'
        self.score -= 1
        if self.score < 0: self.score = 0

    def on_enter_GAME_OVER(self):
        self.media_path = self._MEDIA_FOLDER + 'GAME_OVER.jpg'

    def on_enter_QUESTION(self):
        log.debug('Caricamento nuova domanda...')
        while True:
            media_scopes = list(self._QA.keys())
            random_scope = randint(0, len(media_scopes) - 1)
            self.scope = media_scopes[random_scope]
            random_question = randint(0, len(self._QA[self.scope]) - 1)
            self.qa = self._QA.get(self.scope)[random_question]
            if (self.scope, self.qa.get('path')) not in self._done:
                break
        self._done.add((self.scope, self.qa.get('path')))
        log.debug("TRIVIA: Domanda/risposta caricata: \n{}".format(self.qa))
        self.media_path = self._MEDIA_FOLDER + self.scope + '/' + self.qa.get('path')

    def on_enter_EVAL(self):
        if self.p == self.qa['ans']:
            self.correct()
        else:
            self.incorrect()
    
    def p2a(self, prediction):
        self.p = prediction
        return prediction
    
class Game:

    def __init__(self, predictions: Predictions, display: Display, camera: Camera):
        self.p = predictions
        self.d = display
        self.c = camera
        self.fsm = None
        self.controller = None
        self.shutdown = False
        self._load_FSM('CONTROL')

    def _load_FSM(self, name):
        if self.fsm:
            log.debug('Elimino vecchia FSM caricata...')
            del self.fsm
            del self.controller
        log.debug('Caricamento FSM: {}'.format(name))
        # carica la classe della FSM parametrica rispetto "name"
        _classe = getattr(sys.modules[__name__], name.title())
        states, transitions = _classe._FSM
        log.debug('Stati: {}'.format(states))
        log.debug('Transizioni: {}'.format(transitions))
        self.fsm = _classe()
        self.controller = Machine(model=self.fsm, states=states, initial=states[0])
        for transition in transitions:
            self.controller.add_transition(trigger=transition[0], source=transition[1], dest=transition[2])
        log.debug('FSM {} caricata.'.format(name))

    async def play(self):
        debug_counter = 0
        while True:
            if self.shutdown:
                break
            self.d.display(self.fsm.media_path)
            await sleep(3)
            log.debug('Stato di partenza: {}'.format(self.fsm.state))
            await self.advance()
            log.debug('Stato di arrivo: {}'.format(self.fsm.state))
            debug_counter += 1
            log.debug('Avanzamento n {}'.format(debug_counter))

    async def advance(self):
        log.debug('Avvio avanzamento...')
        action = None
        counter = 0

        # avvia la preview
        self.c.show_preview()
        # finche l'azione non e' ammessa tra quelle nello stato attuale richiedi predizioni
        while action not in self.controller.get_triggers(self.fsm.state):
            # se non ho trovato una azione valida al primo giro mostra un messaggio sul display
            if counter > 0:
                self.d.invalid_action()
            log.debug('Tentativo di selezione della prossima azione n {}'.format(counter))
            log.debug('Azioni attivabili\n{}'.format(
                self.controller.get_triggers(self.fsm.state)))
            # se la transizione da questo stato al successivo necessita di un predizione
            # richiedi una nuova predizione per il gioco corrente
            prediction = self.p.predict(self.fsm.state)
            log.debug('Predizione ottenuta: {}'.format(prediction))
            # mappa la predizione in una azione
            action = self.fsm.p2a(prediction)
            log.debug('Azione selezionata:  {}'.format(action))
            
            counter += 1

        # termina la preview 
        self.c.stop_preview()

        # esegui la transizione verso il nuovo stato
        self.fsm.trigger(action)

        # se il nuovo stato e' finale per CONTROL, carica uno dei giochi
        # if self.fsm.state in ['TRIVIA']:
        #     self._load_FSM(self.fsm.state)

        # se non ci sono stati da poter raggiungere dallo stato attuale, valuta se caricare gioco o uscire
        if len([action for action in self.controller.get_triggers(self.fsm.state) if not action.startswith('to_')]) == 0:
            if self.fsm.name == 'CONTROL' and (self.fsm.state not in ['EXIT']):
                self._load_FSM(self.fsm.state)
            elif self.fsm.state in ['GAME_OVER']:
                self._load_FSM('CONTROL')
            else:
                log.debug('Nessuno stato raggiungibile, uscita.')
                self.shutdown = True
        
        

        
