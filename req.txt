autopep8==1.5.7
numpy==1.21.2
Pillow==8.3.2
pycodestyle==2.7.0
six==1.16.0
toml==0.10.2
transitions==0.8.9
